# README Computerteknologi project 1 


## Table of contents

[[_TOC_]]


## Introduction
This project contains the code, world and launch files required to run a turtle bot burger in Gazebo. This project is mainly a proof of concept for a real driveable robot and to test the software and prepare it for real use in and physical maze. The software should compatible with a real Turtlebot burger. 
## Requirements
This specific software is written an Ubuntu virtual machine and is not tried and tested on any other operating system. ROS is required to run this project since the project relies on some nodes available in ROS.Also the program Gazebo is required as well as the Turtlebot extension for ROS. Also, The simulation will be running a 3D live environment it is therefore recommended to have allocated sufficient processing power to make the simulation run smoothly. 
## Recommended modules (optional)
It is recommended that the full ROS package is downloaded and not just the simple version
## Installation 
To install ros on ubuntu: [Ubuntu ROS installation guide](http://wiki.ros.org/noetic/Installation/Ubuntu).

Follow the link at the bottom to the ROS tutorial. Here it is only necessary to complete the beginner level.

When you are done download Gazebo [here](http://gazebosim.org/download).

Optionally go to turtlebot3 simulations [here](https://emanual.robotis.com/docs/en/platform/turtlebot3/simulation/#gazebo-simulation) make sure you choose noetic in the top, with this you can check and make sure everything functions correctly.
## Configuration
When saving a world created with Gazebo remember to use .world, and optionally put it in the worlds folder in the turtlebot3_gazebo folder, and when you have saved a world file you need a launch file to run the world with ROS, an example of a .launch file:

Launch file example: 

![](img/example_world.PNG)

Here using turtlebot3 Gazebo simulations.
