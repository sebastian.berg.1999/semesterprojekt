
#include "turtlebot3_gazebo/turtlebot3_drive.h"

Turtlebot3Drive::Turtlebot3Drive()
  : nh_priv_("~")
{
  //Init gazebo ros turtlebot3 node
  ROS_INFO("TurtleBot3 Simulation Node Init");
  auto ret = init();
  ROS_ASSERT(ret);
}

Turtlebot3Drive::~Turtlebot3Drive()
{
  updatecommandVelocity(0.0, 0.0);
  ros::shutdown();
}

/*******************************************************************************
* Init function
*******************************************************************************/
bool Turtlebot3Drive::init()
{
  // initialize ROS parameter
  std::string cmd_vel_topic_name = nh_.param<std::string>("cmd_vel_topic_name", "");

  // initialize variables
  check_forward_dist_ = 0.7;
  check_side_dist_    = 0.5;
  check_max_dist_     = 1;
  check_min_dist_     = 0.2;
  check_min_dist2_    = 0.12;



  // initialize publishers
  cmd_vel_pub_   = nh_.advertise<geometry_msgs::Twist>(cmd_vel_topic_name, 10);

  // initialize subscribers
  laser_scan_sub_  = nh_.subscribe("scan", 10, &Turtlebot3Drive::laserScanMsgCallBack, this);

  return true;
}

void Turtlebot3Drive::laserScanMsgCallBack(const sensor_msgs::LaserScan::ConstPtr &msg)
{
  uint16_t scan_angle[8] = {0, 30, 330, 15, 345, 45, 315, 270};

  for (int num = 0; num < 8; num++)
  {
    if (std::isinf(msg->ranges.at(scan_angle[num])))
    {
      scan_data_[num] = msg->range_max;
    }
    else
    {
      scan_data_[num] = msg->ranges.at(scan_angle[num]);
    }
  }
}

void Turtlebot3Drive::updatecommandVelocity(double linear, double angular)
{
  geometry_msgs::Twist cmd_vel;

  cmd_vel.linear.x  = linear;
  cmd_vel.angular.z = angular;

  cmd_vel_pub_.publish(cmd_vel);
}
bool Turtlebot3Drive:: FindDistance(double* vel_acc, double* tim_acc, double* col_acc, double curr_vel)
{
  ros::Rate loop_rate2(125);
  double dist = 1.0;
  while(ros::ok())
  {
    if(scan_data_[CENTER] > dist && scan_data_[CENTER] < 3.49)
    {
      dist = scan_data_[CENTER];
    }
    else if (scan_data_[CENTER] < dist || scan_data_[CENTER] > 3.49)
    {
      return true;
    }
    *tim_acc +=1;
    *vel_acc +=curr_vel;
    if(0.01 < scan_data_[LEFT45] && scan_data_[LEFT45] < check_min_dist2_ || 0.01 < scan_data_[RIGHT45] 
      && scan_data_[RIGHT45] < check_min_dist2_ || 0.01 < scan_data_[CENTER] && scan_data_[CENTER] < check_min_dist2_)
    {
      *col_acc+=1;
    }
    std::cout << "FD: Average linear speed = " << *vel_acc/ *tim_acc << "m/s\n\n";
    std::cout << "FD: Number of collisions = " << *col_acc << "\n\n";
    std::cout << "FD: Time = " << *tim_acc/125.0 << " s\n\n";
    ros::spinOnce();
    loop_rate2.sleep();
  }
  return false;
}
/*******************************************************************************
* Control Loop function
*******************************************************************************/
bool Turtlebot3Drive::controlLoop(double* spd_acc, double* tim_acc, double* col_acc)
{
  static uint8_t turtlebot3_state_num = 0;
  double value = 0.7;
  if(0.01 < scan_data_[LEFT45] && scan_data_[LEFT45] < check_min_dist2_ || 0.01 < scan_data_[RIGHT45] 
      && scan_data_[RIGHT45] < check_min_dist2_ || 0.01 < scan_data_[CENTER] && scan_data_[CENTER] < check_min_dist2_)
  {
    *col_acc+=1;
  }
  switch(turtlebot3_state_num)
  {
    case GET_TB3_DIRECTION:
      if (scan_data_[CENTER] > check_forward_dist_ && scan_data_[LEFT15] > check_side_dist_ && 
      scan_data_[RIGHT15] > check_side_dist_ && scan_data_[LEFT30] > check_min_dist_ && scan_data_[RIGHT30] > check_min_dist_)
      {
        if (scan_data_[RIGHT90] > check_min_dist2_ && scan_data_[RIGHT45] > check_max_dist_ )
        {
          turtlebot3_state_num = TB3_RIGHT_TURN2;
        }
        else if (scan_data_[CENTER] < check_forward_dist_*2 && scan_data_[RIGHT30] < check_max_dist_ && scan_data_[LEFT30]>check_max_dist_)
        {
          turtlebot3_state_num = TB3_LEFT_TURN3;
        }
        else
        {
          turtlebot3_state_num = TB3_DRIVE_FORWARD;
        }
      }
      else if (scan_data_[CENTER] < check_forward_dist_ && scan_data_[LEFT45] < check_side_dist_ && scan_data_[RIGHT45] < check_side_dist_)
      {
        turtlebot3_state_num = TB3_LEFT_TURN;
      }
      else if (scan_data_[CENTER]< check_min_dist_ || scan_data_[LEFT15] < check_min_dist_ || scan_data_[RIGHT15] < check_min_dist_)
      {
        turtlebot3_state_num = TB3_FAILSAFE;
      }
      else if (scan_data_[RIGHT45]<check_forward_dist_ && (scan_data_[CENTER] < check_forward_dist_ ||
       scan_data_[RIGHT45] < check_min_dist_ || scan_data_[RIGHT90]< check_min_dist_))
      {
        turtlebot3_state_num = TB3_LEFT_TURN2;
      }
      else
      {
      turtlebot3_state_num = TB3_LEFT_TURN;
      }
      break;

    case TB3_DRIVE_FORWARD:
      *tim_acc +=1;
      *spd_acc += LINEAR_VELOCITY;
      updatecommandVelocity(LINEAR_VELOCITY, 0.0);
      turtlebot3_state_num = GET_TB3_DIRECTION;
      break;

    case TB3_RIGHT_TURN:
      *tim_acc +=1;
      updatecommandVelocity(0.0, -1* ANGULAR_VELOCITY);
      FindDistance(spd_acc,tim_acc,col_acc,0.0);
      turtlebot3_state_num = GET_TB3_DIRECTION;
      break;

    case TB3_RIGHT_TURN2:
      *tim_acc +=1;
      *spd_acc += value*LINEAR_VELOCITY;
      updatecommandVelocity(value*LINEAR_VELOCITY, -value* ANGULAR_VELOCITY);
      FindDistance(spd_acc,tim_acc,col_acc,value*LINEAR_VELOCITY);
      turtlebot3_state_num = GET_TB3_DIRECTION;
      break;

    case TB3_LEFT_TURN:
      *tim_acc +=1;
      updatecommandVelocity(0.0, ANGULAR_VELOCITY);
      FindDistance(spd_acc,tim_acc,col_acc,0.0);
      turtlebot3_state_num = GET_TB3_DIRECTION;
      break;

    case TB3_LEFT_TURN2:
      *tim_acc +=1;
      *spd_acc += value*LINEAR_VELOCITY;
      updatecommandVelocity(value*LINEAR_VELOCITY, value*ANGULAR_VELOCITY);
      FindDistance(spd_acc,tim_acc,col_acc,value*LINEAR_VELOCITY);
      turtlebot3_state_num = GET_TB3_DIRECTION;
      break;

    case TB3_LEFT_TURN3:
      *tim_acc +=1;
      *spd_acc += LINEAR_VELOCITY;
      updatecommandVelocity(LINEAR_VELOCITY, 0.5*value*ANGULAR_VELOCITY);
      FindDistance(spd_acc,tim_acc,col_acc,LINEAR_VELOCITY);
      turtlebot3_state_num = GET_TB3_DIRECTION;
      break;

    case TB3_FAILSAFE:
      *tim_acc +=1;
      *spd_acc -= 0.5*value*LINEAR_VELOCITY;
      updatecommandVelocity(-0.5*value*LINEAR_VELOCITY, 0);
      turtlebot3_state_num = GET_TB3_DIRECTION;
      break;

    default:
      turtlebot3_state_num = GET_TB3_DIRECTION;
      break;
  }

  return true;
}

/*******************************************************************************
* Main function
*******************************************************************************/
int main(int argc, char* argv[])
{
  ros::init(argc, argv, "turtlebot3_drive");
  Turtlebot3Drive turtlebot3_drive;

  ros::Rate loop_rate(125);
  double speed_accumulation = 0.0;
  double time_accumulation = 0.0;
  double collision_accumulation = 0.0;
  while (ros::ok())
  {
    turtlebot3_drive.controlLoop(&speed_accumulation, &time_accumulation, &collision_accumulation);
    std::cout << "C: Average linear speed = " << speed_accumulation/ time_accumulation << "m/s\n\n";
    std::cout << "C: Number of collisions = " << collision_accumulation << "\n\n";
    std::cout << "C: Time = " << time_accumulation/125.0 << " s\n\n";
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}