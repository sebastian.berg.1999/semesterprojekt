#ifndef TURTLEBOT3_DRIVE_H_
#define TURTLEBOT3_DRIVE_H_

#include <ros/ros.h>

#include <sensor_msgs/LaserScan.h>
#include <geometry_msgs/Twist.h>
#include <nav_msgs/Odometry.h>

#define DEG2RAD (M_PI / 180.0)
#define RAD2DEG (180.0 / M_PI)

#define CENTER 0
#define LEFT30  1
#define RIGHT30 2
#define LEFT15  3
#define RIGHT15 4
#define LEFT45  5
#define RIGHT45 6
#define RIGHT90 7

#define LINEAR_VELOCITY  0.3
#define ANGULAR_VELOCITY 1.5

#define GET_TB3_DIRECTION 0
#define TB3_DRIVE_FORWARD 1
#define TB3_RIGHT_TURN    2
#define TB3_RIGHT_TURN2   3
#define TB3_LEFT_TURN     4
#define TB3_LEFT_TURN2    5
#define TB3_LEFT_TURN3    6
#define TB3_FAILSAFE      7

class Turtlebot3Drive
{
 public:
  Turtlebot3Drive();
  ~Turtlebot3Drive();
  bool init();
  bool FindDistance(double* vel_acc, double* tim_acc, double* col_acc, double curr_vel);
  bool controlLoop(double* spd_acc, double* tim_acc, double* col_acc);

 private:
  // ROS NodeHandle
  ros::NodeHandle nh_;
  ros::NodeHandle nh_priv_;

  // ROS Parameters

  // ROS Time

  // ROS Topic Publishers
  ros::Publisher cmd_vel_pub_;

  // ROS Topic Subscribers
  ros::Subscriber laser_scan_sub_;

  // Variables
  double check_forward_dist_;
  double check_side_dist_;
  double check_max_dist_;
  double check_min_dist_;
  double check_min_dist2_;

  double scan_data_[8] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

  // Function prototypes
  void updatecommandVelocity(double linear, double angular);
  void laserScanMsgCallBack(const sensor_msgs::LaserScan::ConstPtr &msg);
};
#endif // TURTLEBOT3_DRIVE_H_
